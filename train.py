import os
from datetime import datetime
import numpy as np
import argparse

from skimage.transform import resize
from sklearn.model_selection import train_test_split

from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, BatchNormalization, Activation, Dropout
from tensorflow.keras.layers import Conv2D, Conv2DTranspose
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import concatenate
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import img_to_array, load_img, save_img


im_width = 128
im_height = 128


def get_data(path, train=True):
    ids = next(os.walk(path + 'x'))[2]
    ids = ids[:(len(ids)//4)]
    X = np.zeros((len(ids), im_height, im_width, 1), dtype=np.float32)
    if train:
        y = np.zeros((len(ids), im_height, im_width, 1), dtype=np.float32)
    print('Getting and resizing images ... ')
    for n, id_ in enumerate(ids):
        # Load images
        img = load_img(path + '/x/' + id_, color_mode='grayscale')
        x_img = img_to_array(img)
        x_img = resize(x_img, (im_height, im_width, 1), mode='constant', preserve_range=True)

        # Load masks
        if train:
            mask = img_to_array(load_img(path + '/y/' + id_, color_mode='grayscale'))
            mask = resize(mask, (im_height, im_width, 1), mode='constant', preserve_range=True)

        # Save images
        X[n, ..., 0] = x_img.squeeze() / 255
        if train:
            y[n] = mask / 255
        if n != 0 and n % 500 == 0:
            print('500 Done')
    print('Done!')
    if train:
        return X, y
    else:
        return X


def conv2d_block(input_tensor, n_filters, kernel_size=3, batchnorm=True):
    # first layer
    x = Conv2D(filters=n_filters, kernel_size=(kernel_size, kernel_size), kernel_initializer="he_normal",
               padding="same")(input_tensor)
    if batchnorm:
        x = BatchNormalization()(x)
    x = Activation("relu")(x)
    # second layer
    x = Conv2D(filters=n_filters, kernel_size=(kernel_size, kernel_size), kernel_initializer="he_normal",
               padding="same")(x)
    if batchnorm:
        x = BatchNormalization()(x)
    x = Activation("relu")(x)
    return x


def get_unet(input_img, n_filters=16, dropout=0.5, batchnorm=True):
    # contracting path
    c1 = conv2d_block(input_img, n_filters=n_filters * 1, kernel_size=3, batchnorm=batchnorm)
    p1 = MaxPooling2D((2, 2))(c1)
    p1 = Dropout(dropout * 0.5)(p1)

    c2 = conv2d_block(p1, n_filters=n_filters * 2, kernel_size=3, batchnorm=batchnorm)
    p2 = MaxPooling2D((2, 2))(c2)
    p2 = Dropout(dropout)(p2)

    c3 = conv2d_block(p2, n_filters=n_filters * 4, kernel_size=3, batchnorm=batchnorm)
    p3 = MaxPooling2D((2, 2))(c3)
    p3 = Dropout(dropout)(p3)

    c4 = conv2d_block(p3, n_filters=n_filters * 8, kernel_size=3, batchnorm=batchnorm)
    p4 = MaxPooling2D(pool_size=(2, 2))(c4)
    p4 = Dropout(dropout)(p4)

    c5 = conv2d_block(p4, n_filters=n_filters * 16, kernel_size=3, batchnorm=batchnorm)

    # expansive path
    u6 = Conv2DTranspose(n_filters * 8, (3, 3), strides=(2, 2), padding='same')(c5)
    u6 = concatenate([u6, c4])
    u6 = Dropout(dropout)(u6)
    c6 = conv2d_block(u6, n_filters=n_filters * 8, kernel_size=3, batchnorm=batchnorm)

    u7 = Conv2DTranspose(n_filters * 4, (3, 3), strides=(2, 2), padding='same')(c6)
    u7 = concatenate([u7, c3])
    u7 = Dropout(dropout)(u7)
    c7 = conv2d_block(u7, n_filters=n_filters * 4, kernel_size=3, batchnorm=batchnorm)

    u8 = Conv2DTranspose(n_filters * 2, (3, 3), strides=(2, 2), padding='same')(c7)
    u8 = concatenate([u8, c2])
    u8 = Dropout(dropout)(u8)
    c8 = conv2d_block(u8, n_filters=n_filters * 2, kernel_size=3, batchnorm=batchnorm)

    u9 = Conv2DTranspose(n_filters * 1, (3, 3), strides=(2, 2), padding='same')(c8)
    u9 = concatenate([u9, c1], axis=3)
    u9 = Dropout(dropout)(u9)
    c9 = conv2d_block(u9, n_filters=n_filters * 1, kernel_size=3, batchnorm=batchnorm)

    outputs = Conv2D(1, (1, 1), activation='sigmoid')(c9)
    model = Model(inputs=[input_img], outputs=[outputs])
    return model


def main():
    parser = argparse.ArgumentParser(description='Unet train')
    parser.add_argument('-t', action='store', dest='train_path', help='Path to the train data')
    parser.add_argument('-w', action='store', dest='im_width', help='Img width')
    parser.add_argument('-he', action='store', dest='im_height', help='Img height')
    parser.add_argument('-v', action='store', dest='valid_path', help='Path to the validation data')
    parser.add_argument('-p', action='store', dest='predict', help='Predict')
    args = parser.parse_args()

    im_width = args.im_width
    im_height = args.im_height
    if args.predict:
        x = get_data(args.predict, train=False)
        input_img = Input((im_height, im_width, 1), name='img')
        model = get_unet(input_img, n_filters=16, dropout=0.05, batchnorm=True)
        model.load_weights("model-tgs-salt.h5")
        model.compile(optimizer=Adam(), loss="binary_crossentropy", metrics=["accuracy"])
        result = model.predict(x)
        for idx, i in enumerate(result):
            save_img('{}/y/{}.jpeg'.format(args.predict, idx), i)
    else:
        X_train, y_train = get_data(args.train_path, train=True)
        if args.get('valid_path', False):
            X_valid, y_valid = get_data(args.valid_path, train=True)
        else:
            X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.15, random_state=2018)

        input_img = Input((im_height, im_width, 1), name='img')
        model = get_unet(input_img, n_filters=16, dropout=0.05, batchnorm=True)
        model.load_weights("model-tgs-salt.h5")
        model.compile(optimizer=Adam(), loss="binary_crossentropy", metrics=["accuracy"])
        model.summary()
        tensorboard = TensorBoard(log_dir='logs/{}'.format(datetime.now().strftime("%Y-%m-%d-%H:%M:%S")))
        callbacks = [
            EarlyStopping(patience=10, verbose=1),
            ReduceLROnPlateau(factor=0.1, patience=3, min_lr=0.00001, verbose=1),
            ModelCheckpoint('model-tgs-salt.h5', verbose=1, save_best_only=True, save_weights_only=True),
            tensorboard
        ]
        results = model.fit(X_train, y_train, batch_size=32, epochs=50, callbacks=callbacks,
                        validation_data=(X_valid, y_valid))


if __name__ == '__main__':
    main()
