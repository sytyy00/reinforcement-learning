from PIL import Image

image = Image.open('asd.jpg')
image.show()
key_location = {'1': (56, 14, 84, 42), '2': (98, 14, 126, 42), '3': (144, 14, 170, 42), '4': (188, 14, 212, 42),
                '5': (233, 14, 262, 42), 'tab': (14, 56, 69, 90), 'caps': (14, 99, 79, 134),
                'shift': (14, 143, 101, 178), 'ctrl': (14, 186, 70, 224), 'alt': (122, 186, 179, 222),
                'space': (192, 187, 301, 222), 'lbm': (307, 18, 345, 53), 'rbm': (362, 18, 399, 53),
                'q': (79, 55, 113, 91), 'w': (127, 56, 155, 85), 'e': (169, 56, 202, 90), 'r': (212, 56, 246, 90),
                't': (255, 56, 290, 90), 'a': (92, 99, 126, 134), 's': (136, 99, 171, 133), 'd': (179, 99, 214, 135),
                'f': (223, 99, 258, 134), 'g': (266, 99, 302, 135), 'z': (112, 143, 147, 179),
                'x': (155, 143, 192, 180), 'c': (199, 143, 236, 180), 'v': (244, 143, 279, 180), }
result = {}
for key_name, box in key_location.items():
    key_frame = image.crop(box=box)
    pixel_colors = sorted(key_frame.getcolors(key_frame.size[0]*key_frame.size[1]), key=lambda x: x[0], reverse=True)[0]
    if all([True if i >= 200 else False for i in pixel_colors[1]]):
        result[key_name] = 1
    else:
        result[key_name] = 0
print(result)
