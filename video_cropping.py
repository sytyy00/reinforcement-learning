import cv2
import argparse


def copping_video():
    parser = argparse.ArgumentParser(description='Parser video')
    parser.add_argument('-v', action='store', dest='v', help='Path to the video')
    parser.add_argument('-x', action='store', dest='x', help='Path for X')
    parser.add_argument('-y', action='store', dest='y', help='Path for Y')
    args = parser.parse_args()

    cap = cv2.VideoCapture(args.v)
    i = 0
    count = 0
    while (cap.isOpened()):
        ret, frame = cap.read()
        if ret == True:
            if count % 5 == 0:
                gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                in_frame = gray_frame[0:1020, 0:1280]
                out_frame = gray_frame[485:1020, 1300:1920]
                if cv2.countNonZero(out_frame) < 100:# убирать типо чёрные изображения
                    continue
                cv2.imwrite(args.x + str(i) + '.jpg', in_frame)
                cv2.imwrite(args.y + str(i) + '.jpg', out_frame)
                i += 1
                #flip horizontal
                cv2.imwrite(args.x + str(i) + '.jpg', cv2.flip(in_frame, 1))
                cv2.imwrite(args.y + str(i) + '.jpg', cv2.flip(out_frame, 1))
                i += 1
                # rotate
                cv2.imwrite(args.x + str(i) + '.jpg', cv2.rotate(in_frame, cv2.ROTATE_90_CLOCKWISE))
                cv2.imwrite(args.y + str(i) + '.jpg', cv2.rotate(out_frame, cv2.ROTATE_90_CLOCKWISE))
                i += 1
            count += 1
        else:
            break
    cap.release()
    cv2.destroyAllWindows()
    print('Done')


if __name__ == '__main__':
    copping_video()
    # copping_video('video/1.mp4', 'data/x/', 'data/y/')
